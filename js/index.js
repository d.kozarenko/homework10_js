"use strict";

const icon = document.querySelectorAll("#icon");
const form = document.querySelector("#form");
const firstInput = document.querySelector("#first-input");
const secondInput = document.querySelector("#second-input");
const error = document.createElement("span");
form.addEventListener("submit", formCheckingHandler);
icon.forEach((element) => {
  element.addEventListener("click", iconClickingHandler);
});

function iconClickingHandler(event) {
  const currentInput = document.querySelector(
    '[data-name = "' + this.dataset.iconName + '"]'
  );
  if (currentInput.getAttribute("type") === "password") {
    this.classList.remove("fa-eye");
    this.classList.add("fa-eye-slash");
    currentInput.setAttribute("type", "text");
  } else {
    this.classList.remove("fa-eye-slash");
    this.classList.add("fa-eye");
    currentInput.setAttribute("type", "password");
  }
}

function formCheckingHandler(event) {
  event.preventDefault();
  if (firstInput.value === secondInput.value && firstInput.value !== "") {
    accessAllowed();
  } else {
    wrongPassword();
  }
}

function accessAllowed() {
  if (error) {
    error.textContent = "";
  }
  alert("You are welcome!");
}

function wrongPassword() {
  error.classList.add("red-font");
  secondInput.after(error);
  error.textContent = "Нужно ввести одинаковые значения";
}
